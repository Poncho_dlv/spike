#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import discord
from discord.ext.commands import command, guild_only, Bot, Cog, check

from __init__ import spike_logger, spike_command_logger, except_hook, intents
from spike_command.Bounty import Bounty as CmdBounty
from spike_command.Common import Common as CmdCommon
from spike_command.Competition import Competition
from spike_command.Schedule import Schedule
from spike_command.Statistics import Statistics as CmdStatistics
from spike_event.EventCommon import EventCommon
from spike_settings.SpikeSettings import SpikeSettings
from discord_resources.Utilities import DiscordUtilities

# Discord client
client = Bot(command_prefix="s!", intents=intents)

Already_running = False


async def log_command(ctx):
    abuse_list = SpikeSettings.get_abuse_list()
    if ctx.author.id in abuse_list:
        return False
    if ctx.guild is not None:
        spike_command_logger.info("{} :: {}".format(ctx.command, ctx.guild.name))
    else:
        spike_command_logger.info("{} :: Private message".format(ctx.command))
    return True
    

class Common(Cog):
    """Users commands documentation"""
    @command(aliases=['info'])
    @check(log_command)
    async def coach(self, ctx, *, args):
        """Display coach information on discord.

            Mandatory argument:
                Coach name approximate name of a BB2 coach
            Optional argument:
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game
            Example:
                s!coach poncho
        """
        cmd = CmdCommon(client)
        await cmd.coach_information(ctx, args)

    @command(aliases=['report'])
    @check(log_command)
    async def match(self, ctx, *, match_id):
        """Display a match report on discord.

            Mandatory argument:
                Match id   cyanide id of the match
            Example:
                s!report 10004d7d31
        """
        cmd = CmdCommon(client)
        await cmd.report(ctx, match_id)

    @command()
    @check(log_command)
    async def team(self, ctx, *, args):
        """Display a team report on discord.

            Mandatory argument:
                Team name   Full name of the team

            Optional argument:
                Output option
                    -tv     Show value of each player
                    -atr    Show attributes of each player
                    -id     Display team and player id
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game

            Example:
                s!team chaos all star
                s!team chaos all star -tv
                s!team chaos all star -tv -atr
                s!team chaos all star pc -atr
        """
        cmd = CmdCommon(client)
        await cmd.team(ctx, args)

    @command()
    @check(log_command)
    @guild_only()
    async def rank(self, ctx, *, competition_name, platform=None):
        """Display the competition top 20 ranking

            Mandatory argument:
                competition_name:   Approximate name of the competition
            Example:
                s!rank champ ladder

        """
        cmd = Competition(client)
        await cmd.rank(ctx, competition_name)

    @command()
    @check(log_command)
    async def roll(self, ctx, dice):
        """Display dice roll on discord.

            Mandatory argument:
                dice   dice to roll
            Example:
                s!roll 1d6
                s!roll 2d3
                s!roll 5d20
                s!roll 2db
        """
        cmd = CmdCommon(client)
        await cmd.roll(ctx, dice)


class Bounty(Cog):
    """Bounty documentations"""

    @command(aliases=['bounty'])
    @check(log_command)
    async def bnty(self, ctx):
        """Display the list of all your bounty"""
        cmd = CmdBounty(client)
        await cmd.bounty_list(ctx)


class Scheduling(Cog):
    """Scheduling documentations"""

    @command()
    @check(log_command)
    @guild_only()
    async def sched(self, ctx, *, args):
        """Display scheduled match for the given competition

            Mandatory argument:
                Competition name: approximate name of registered competition

            Optional argument:
                Output option
                    -t      Display team name
                    -bet    Display betting reaction 1 X 2
                    -m      Mention coaches with their discord account
                    -d      Display planned date
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game

                Example:
                    s!sched champ cup pc
                    s!sched champ cup -l
                    s!sched champ cup -bet -t
                    s!sched champ cup -bet ps4
        """
        cmd = Schedule(client)
        await cmd.schedule_competition(ctx, args)

    @command()
    @check(log_command)
    @guild_only()
    async def schedL(self, ctx, *, args):
        """Display scheduled match for the given league

            Mandatory argument:
                League name: approximate name of registered league

            Optional argument:
                Output option
                    -t      Display team name
                    -bet    Display betting reaction 1 X 2
                    -m      Mention coaches with their discord account
                    -d      Display planned date
                Platform: (By default use league platform)
                    pc      Team is in PC game
                    ps4     Team is in Ps4 game
                    xb1     Team is in xBox game

                Example:
                    s!schedL mad ac
        """
        cmd = Schedule(client)
        await cmd.schedule_league(ctx, args)

    @command()
    @check(log_command)
    async def mysched(self, ctx):
        """Display all my planned matches

                Example:
                    s!mysched

        """
        cmd = Schedule(client)
        await cmd.my_sched(ctx)

    @command()
    @check(log_command)
    @guild_only()
    async def motd(self, ctx):
        """Display all planned matches of the current day

                Example:
                    s!motd
        """
        cmd = Schedule(client)
        await cmd.match_of_the_day(ctx)

    @command()
    @check(log_command)
    async def plan(self, ctx, *, args):
        """Planify a match

            Mandatory argument:
                Opponent name:  approximate name of the opponent coach
                Date:           date of the match DD/MM :: HH:MM

            Optional argument:
                Platform: (By default use league platform)
                    pc      Match is in PC game
                    ps4     Match is in Ps4 game
                    xb1     Match is in xBox game

                Example:
                    s!plan meshu 21/03 20:30

        """
        cmd = Schedule(client)
        await cmd.plan_match(ctx, args)


class Miscellaneous(Cog):
    """Miscellaneous documentations"""

    @command()
    @check(log_command)
    async def invite(self, ctx):
        """Display a message to invite Spike! on your discord server and give a link to Spike discord."""
        cmd = CmdCommon(client)
        await cmd.invite(ctx)

    @command()
    @check(log_command)
    async def join(self, ctx):
        """Link to Spike discord."""
        cmd = CmdCommon(client)
        await cmd.join(ctx)


class Statistics(Cog):
    @command()
    @check(log_command)
    async def vs(self, ctx, *, args):
        """Display opposition between two coach

            Mandatory argument:
                coach 1: Name of first coach to compare
                coach 2: Name of second coach to compare

            Optional argument:
                Platform: (By default use league platform)
                    pc
                    ps4
                    xb1

                Example:
                    s!vs enarion ducke
                    s!vs spartako elyod
                    s!vs FantFox Durrun ps4
        """
        cmd = CmdStatistics()
        await cmd.versus(ctx, args)

    @command()
    @check(log_command)
    async def wr(self, ctx, *, args):
        """Display win rate of coach

            Mandatory argument:
                Coach name: Name of the coach

            Optional argument:
                Race name: Name or short name of the race
                Platform: (By default use league platform)
                    pc
                    ps4
                    xb1

                Example:
                    s!wr Kfoged
                    s!wr enarion, flings
                    s!wr FantFox ps4
        """
        cmd = CmdStatistics()
        await cmd.win_rate(ctx, args)

    @check(log_command)
    @command(aliases=['ccl'])
    async def cclwr(self, ctx, *, args):
        """Display win rate of coach over all ranked ladder season

            Mandatory argument:
                Coach name: Name of the coach

            Optional argument:
                Race name: Name or short name of the race
                Platform: (By default use league platform)
                    pc
                    ps4
                    xb1

                Example:
                    s!cclwr Kfoged
                    s!cclwr enarion, flings
                    s!cclwr FantFox ps4
        """
        cmd = CmdStatistics()
        await cmd.win_rate(ctx, args, "ccl_win_rate")

    @command()
    @check(log_command)
    async def ccltop(self, ctx, *, args):
        """Display top 10 race of current ranked ladder

            Mandatory argument:
                Race name: Name or short name of the race

            Optional argument:
                Platform: (By default use league platform)
                    pc
                    ps4
                    xb1

                Example:
                    s!ccltop rats
                    s!ccltop chorf ps4
        """
        cmd = CmdStatistics()
        await cmd.top_race(ctx, args)


async def process_output(ctx, output):
    utils = DiscordUtilities(client)
    for message in output:
        await utils.send_custom_message(ctx.message.channel, message)
    await utils.delete_message(ctx.message)


def main():
    client.run(SpikeSettings.get_patreon_discord_token())


@client.event
async def on_command_error(ctx, error):
    event = EventCommon(client)
    await event.on_command_error(ctx, error, "s!")


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        await client.add_cog(Common())
        await client.add_cog(Bounty())
        await client.add_cog(Scheduling())
        await client.add_cog(Statistics())
        await client.add_cog(Miscellaneous())
        spike_logger.info("user_bot connected")
        Already_running = True
        await client.change_presence(activity=discord.Game("s!help"))
    else:
        spike_logger.warning("user_bot reconnect")


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
