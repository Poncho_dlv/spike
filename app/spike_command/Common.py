#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import dice as m_dice
import discord
import pyshorteners

from __init__ import spike_logger
from discord_resources.Resources import Resources
from discord_resources.Utilities import DiscordUtilities
from discord_write.Match import Match
from discord_write.Team import Team
from spike_command.BaseCommand import BaseCommand
from spike_database.Coaches import Coaches
from spike_database.DiscordGuild import DiscordGuild
from spike_database.MatchHistory import MatchHistory
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import CoachNotFoundError, DefaultError, TeamNotFoundError, InvalidSyntaxError, CompetitionNotFoundError
from spike_model.Coach import Coach
from spike_requester.CyanideApi import CyanideApi
from spike_requester.SpikeAPI.StatisticsAPI import StatisticsAPI
from spike_requester.Utilities import Utilities as RequesterUtils
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from spike_utilities.FuzzyUtils import FuzzyUtils
from spike_utilities.Utilities import Utilities
from spike_user_db.Users import Users


class Common(BaseCommand):

    def __init__(self, client):
        super(Common, self).__init__(client)
        self.__youtube_base_url = 'https://www.youtube.com/channel/'
        self.__twitch_base_url = 'https://www.twitch.tv/'

    async def coach_information(self, ctx, args):
        channel = ctx.message.channel
        utils = DiscordUtilities(self.client)
        match_history_db = MatchHistory()
        coach_db = Coaches()
        user_db = Users()
        common_db = ResourcesRequest()
        rsrc = Resources()
        thumbnail = None

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        args = args.split(' ')
        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if platform == 'all':
            platform = 'pc'

        platform_id = common_db.get_platform_id(platform)

        searched_name = ' '.join(args)
        fuzzy = FuzzyUtils()

        like_coach = coach_db.get_coaches_like(searched_name, platform_id)
        coach_name = None
        if len(like_coach) > 0:
            if len(like_coach) == 1:
                coach_name = like_coach[0]
            else:
                res = fuzzy.get_best_match_into_list(searched_name, like_coach)
                if res is not None:
                    coach_name = res[0]

            if coach_name is not None:
                coach_data = coach_db.get_coach_data(coach_name, platform_id)
                user_linked = user_db.get_user_linked(coach_data['id'], coach_data['platform_id'])
                if user_linked is not None:
                    user_linked = user_db.get_user(str(user_linked))
                    if ctx.guild:
                        discord_member = ctx.guild.get_member(user_linked.get('discord_id'))
                    else:
                        discord_member = None

                    if discord_member is None:
                        discord_user = self.client.get_user(user_linked.get('discord_id'))
                        if discord_user is None:
                            discord_user = await self.client.fetch_user(user_linked.get('discord_id'))
                        if discord_user is not None:
                            thumbnail = discord_user.display_avatar.url
                            description = '{} {}'.format(rsrc.get_discord_emoji(), discord_user.display_name)
                        else:
                            description = ""
                            spike_logger.error('Invalid user data: {}'.format(user_linked.get('_id')))
                    else:
                        thumbnail = discord_member.display_avatar.url
                        description = '{} {}'.format(rsrc.get_discord_emoji(), discord_member.mention)
                else:
                    # /* Link to my discord */
                    link_message = Translator.tr('#_common_command.link_bb2_discord', settings.get_language())
                    description = '[{}](https://spike.ovh/link_coach?coach_id={}&platform_id={})'.format(link_message, coach_data['id'], coach_data['platform_id'])

                coach_url = 'https://spike.ovh/coach?coach_id={}&platform_id={}'.format(coach_data['id'], coach_data['platform_id'])

                if user_linked and user_linked.get('steam', {}).get('profile_url'):
                    description += '\n{} [{}]({})'.format(rsrc.get_steam_emoji(), user_linked.get('steam', {}).get('name'), user_linked.get('steam', {}).get('profile_url'))

                if 'other_names' in coach_data.keys():
                    for name in coach_data['other_names']:
                        if name != coach_data['name']:
                            description += '\n{} {}'.format(rsrc.get_blood_bowl_2_emoji(), name)

                description += '\n \u200b'

                embed = discord.Embed(title=coach_name, url=coach_url, description=description, color=0x992d22)

                if thumbnail is not None:
                    embed.set_thumbnail(url=thumbnail)
                else:
                    embed.set_thumbnail(url='http://images.bb2.cyanide-studio.com/misc/II.png')

                info = ""
                if user_linked and user_linked.get('country'):
                    country_flag = rsrc.get_flag_emoji(user_linked['country'])
                    # /* Country: {country_flag} */
                    info += Translator.tr('#_common_command.country', settings.get_language()).format(country_flag=country_flag)
                    info += '\n'

                if user_linked and user_linked.get('lang'):
                    if type(user_linked.get('lang', [])) == list:
                        info += Translator.tr('#_common_command.language', settings.get_language()).format(language="")
                        for lang in user_linked.get('lang', []):
                            lang_id = rsrc.get_flag_emoji(lang)
                            # /* Lang: {language} */
                            info += lang_id + ' '

                if info != "":
                    info += '\n \u200b'
                    # /* Information */
                    embed.add_field(name=Translator.tr('#_common_command.information', settings.get_language()), value=info)

                videos = ""
                if user_linked and user_linked.get('youtube'):
                    videos += '{}[Youtube]({}{})\n'.format(rsrc.get_youtube_emoji(), self.__youtube_base_url, user_linked['youtube'])
                if user_linked and user_linked.get('twitch'):
                    videos += '{}[Twitch]({}{})\n'.format(rsrc.get_twitch_emoji(), self.__twitch_base_url, user_linked['twitch'])

                if videos != "":
                    videos += '\u200b'
                    # /* Videos */
                    embed.add_field(name=Translator.tr('#_common_command.videos_title', settings.get_language()), value=videos)

                try:
                    # Last game field
                    last_match = match_history_db.get_last_match(coach_data['id'], platform_id, 5)
                    last_match = list(last_match)
                    match_list = ""

                    for match in last_match:
                        home_coach = coach_db.get_coach_name(match['team_home']['coach_id'], platform_id)
                        home_logo = common_db.get_team_emoji(match['team_home']['logo'])
                        away_coach = coach_db.get_coach_name(match['team_away']['coach_id'], platform_id)
                        away_logo = common_db.get_team_emoji(match['team_away']['logo'])

                        match_list += '\n{} {} [{} - {}](https://spike.ovh/match?match_uuid={}) {} {}'.format(home_coach, home_logo, match['team_home']['score'], match['team_away']['score'], match['_id'], away_logo, away_coach)

                    # /* Last Match */
                    last_match_title = Translator.tr('#_common_command.last_match_title', settings.get_language())
                    if match_list != "":
                        match_list += '\n \u200b'
                        embed.add_field(name=last_match_title, value=match_list, inline=False)
                    else:
                        # /* No match found*/
                        embed.add_field(name=last_match_title, value=Translator.tr('#_common_command.no_match_found_error', settings.get_language()), inline=False)
                except:
                    name = Translator.tr('#_common_command.last_match_title', settings.get_language())
                    value = Translator.tr('#_common_command.no_match_found_error', settings.get_language())
                    embed.add_field(name=name, value=value, inline=False)

                statistics = StatisticsAPI.get_win_rate(coach_data['id'], coach_data['platform_id'], top_race_limit=5, top_league_limit=5)

                if statistics and statistics.get('nb_match', 0) > 0:
                    # Stat field
                    wr = statistics['win_rate']
                    win = statistics['nb_win']
                    draw = statistics['nb_draw']
                    loss = statistics['nb_loss']
                    tot = statistics['nb_match']
                    # /* **Win rate:** {win_rate}% ({number_of_game} games)\n**Record:** {win}-{draw}-{loss} */
                    stat_output = Translator.tr('#_common_command.win_rate_coach', settings.get_language()).format(win_rate=wr, number_of_game=tot, win=win, draw=draw, loss=loss)
                    # /* games */
                    games_label = Translator.tr('#_common_command.games_label', settings.get_language())
                    stat_output += '\n\n'
                    # /* **Top 5 races:** */
                    stat_output += Translator.tr('#_common_command.top_races_title', settings.get_language())
                    for race in statistics.get('top_races', []):
                        rwr = race['win_rate']
                        rwin = race['nb_win']
                        rdraw = race['nb_draw']
                        rloss = race['nb_loss']
                        rtot = race['nb_match']

                        stat_output += '\n - **{}** {}% ({} {}) {}-{}-{}'.format(race['name'], rwr, rtot, games_label, rwin, rdraw, rloss)

                    stat_output += '\n\n'
                    # /* **Top 5 leagues:** */
                    stat_output += Translator.tr('#_common_command.top_leagues_title', settings.get_language())
                    for league in statistics.get('top_leagues', []):
                        rwr = league['win_rate']
                        rwin = league['nb_win']
                        rdraw = league['nb_draw']
                        rloss = league['nb_loss']
                        rtot = league['nb_match']
                        stat_output += '\n - **{}** {}% ({} {}) {}-{}-{}'.format(league['name'], rwr, rtot, games_label, rwin, rdraw, rloss)

                    # /* Statistics */
                    embed.add_field(name=Translator.tr('#_common_command.statistics_title', settings.get_language()), value=stat_output)
                else:
                    name = Translator.tr('#_common_command.statistics_title', settings.get_language())
                    value = Translator.tr('#_common_command.no_match_found_error')
                    embed.add_field(name=name, value=value, inline=False)

                coach_id = coach_data['id']
                # /* Coach_id: {coach_id} - {platform} */
                embed.set_footer(text=Translator.tr('#_common_command.coach_id_footer', settings.get_language()).format(coach_id=coach_id, platform=platform))

                await utils.send_custom_message(channel, embed=embed)
            else:
                raise CoachNotFoundError(settings.get_language(), searched_name, platform)
        else:
            raise CoachNotFoundError(settings.get_language(), searched_name, platform)

        await utils.delete_message(ctx.message)

    async def roll(self, ctx, dice):
        rsrc = Resources()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')
        roll_output = ""
        utils = DiscordUtilities(self.client)
        try:
            if dice[-2:] == 'db':
                dice_block_number = int(dice.replace('db', ""))
                if dice_block_number > 0:
                    result = m_dice.roll('{}d6'.format(dice_block_number))
                    for res in result:
                        roll_output += ' {}'.format(rsrc.get_block_dice(res))
                    # /* **Block dice roll:** {dice} by **{author}** \n{result} */
                    await utils.send_custom_message(ctx.message.channel, Translator.tr('#_common_command.block_dice_roll', settings.get_language()).format(dice=dice, author=ctx.message.author.name, result=roll_output))
                else:
                    # /* Red dice not supported yet */
                    await utils.send_custom_message(ctx.message.channel, Translator.tr('#_common_command.red_dice_not_supported', settings.get_language()))
            else:
                result = m_dice.roll(dice)

                total = 0
                for res in result:
                    total += res
                    if roll_output != "":
                        roll_output += ' - '
                    roll_output += str(res)
                # /* **Dice roll:** {dice} by: **{author}** \n```ml\nResult: {result}  \nTotal: {total}``` */
                await utils.send_custom_message(ctx.message.channel, Translator.tr('#_common_command.standard_dice_roll', settings.get_language()).format(dice=dice, author=ctx.message.author.name, result=roll_output, total=total))
        except Exception as e:
            spike_logger.error('Error in roll: {}'.format(e))
            # /* Invalid dice: {dice} */
            raise DefaultError(Translator.tr('#_common_command.invalid_dice_roll', settings.get_language()).format(dice=dice))
        await utils.delete_message(ctx.message)

    async def team(self, ctx, args):
        channel = ctx.message.channel
        utils = DiscordUtilities(self.client)
        rsrc_db = ResourcesRequest()
        input_args = args
        args = args.split(' ')
        display_attribute = False
        display_tv = False
        display_id = False
        display_bnty = True

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        platform = Utilities.get_platform_in_args(args)

        if '-tv' in args:
            display_tv = True
            # Remove -tv from args if specified
            args.remove('-tv')

        if '-atr' in args:
            display_attribute = True
            # Remove -atr from args if specified
            args.remove('-atr')

        if '-id' in args:
            display_id = True
            # Remove -id from args if specified
            args.remove('-id')

        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if platform == 'all':
            platform = 'pc'

        platform_id = rsrc_db.get_platform_id(platform)

        if len(args) > 0:
            team_name = ' '.join(args)
            try:
                team_id = int(team_name)
                team = RequesterUtils.get_team(team_id=team_id, platform_id=platform_id)
            except ValueError:
                team = RequesterUtils.get_team(team_name=team_name, platform_id=platform_id)

            if team is not None and team is not False:
                team_writer = Team(self.client, channel)
                await team_writer.write_team(ctx, team, settings.get_language(), platform, display_attribute, display_tv, display_id, display_bnty)
            else:
                raise TeamNotFoundError(settings.get_language(), team_name, platform)
        else:
            raise InvalidSyntaxError(settings.get_language(), input_args)

        await utils.delete_message(ctx.message)

    async def report(self, ctx, match_id):
        channel = ctx.channel
        discord_utils = DiscordUtilities(self.client)

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        try:
            match_id = str(match_id).lower()
            spike_logger.debug('uuid: ' + match_id)
            match = RequesterUtils.get_match(match_id)
            if match is not None:
                if isinstance(channel, discord.abc.PrivateChannel):
                    report_writer = Match(self.client)
                else:
                    report_writer = Match(self.client, ctx.guild.id)

                try:
                    # TODO check if contest_updated if not update_rank=True
                    await report_writer.write_data(match, False, True, channel, True)
                except Exception as e:
                    # /* Unable to display this report */
                    await discord_utils.send_custom_message(channel, Translator.tr('#_common_command.unable_to_display_match_report', settings.get_language()))
                    spike_logger.error('write_data :: match {} :: error: {}'.format(match.get_uuid(), e))
            else:
                await discord_utils.send_custom_message(channel, Translator.tr('#_common_command.unable_to_display_match_report', settings.get_language()))
        except Exception as e:
            spike_logger.error('Error in report: {}'.format(e))
            spike_logger.exception('Unexpected error: ' + str(sys.exc_info()[0]))
            raise
        await discord_utils.delete_message(ctx.message)

    async def coaches(self, ctx, args):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        db = DiscordGuild(ctx.guild.id)
        coach_db = Coaches()
        user_db = Users()
        common_db = ResourcesRequest()
        display_id = False
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        platform = None
        competition_name = args
        args = args.split(' ')

        if '-id' in args:
            display_id = True
            # Remove -id from args if specified
            args.remove('-id')

        if len(args) >= 2:
            if common_db.get_platform_id(args[-1]) is not None:
                platform = args[-1]
                competition_name = competition_name.replace(args[-1], "")

        if platform is None:
            platform = settings.get_platform()

        if platform == 'all':
            platform = 'pc'

        platform_id = common_db.get_platform_id(platform)

        # Get all competition to fuzzy the best match
        competitions = db.get_reported_competitions_label(platform_id)
        fuzzy = FuzzyUtils()
        comp_name = fuzzy.get_best_match_into_list(competition_name, competitions)
        if comp_name is not None:
            competition_name = comp_name[0]
            competition_id = db.get_competition_id(competition_name, platform_id)
            if competition_id is not None:
                competition_data = db.get_competition_data(competition_id)
                league_name = db.get_league_name(competition_data[7], platform_id)

                platform_data = common_db.get_platform(platform_id)
                coaches = CyanideApi.get_coaches(league_name, competition_name, platform=platform_data[1])
                if coaches:
                    nb_coach = len(coaches.get('coaches', []))
                    if nb_coach > 0:
                        # /* {logo} __**{number_of_coach} coaches in {competition}**__ {logo} */
                        output_msg = Translator.tr('#_common_command.coach_list_header', settings.get_language()).format(logo=competition_data[5], number_of_coach=nb_coach, competition=competition_name)
                        sorted_list = []
                        for coach in coaches['coaches']:
                            coach = Coach(coach)
                            coach_db.add_or_update_coach(coach.get_id(), coach.get_name(), platform_id)
                            coach_data = coach_db.get_coach_data(coach.get_name(), platform_id)

                            user_linked = user_db.get_user_linked(coach_data['id'], coach_data['platform_id'])
                            user_display = None
                            if user_linked is not None:
                                user_linked = user_db.get_user(str(user_linked))
                                if ctx.guild:
                                    discord_member = ctx.guild.get_member(user_linked.get('discord_id'))
                                else:
                                    discord_member = None

                                if discord_member is None:
                                    discord_user = self.client.get_user(user_linked.get('discord_id'))
                                    if discord_user is None:
                                        discord_user = await self.client.fetch_user(user_linked.get('discord_id'))
                                    if discord_user:
                                        user_display = discord_user.display_name
                                else:
                                    user_display = discord_member.mention
                            else:
                                sorted_list.append('{} - *{}*'.format(coach.get_name(), coach.get_id()))

                            if user_display:
                                if display_id:
                                    sorted_list.append('{} - *{}* - {}'.format(coach.get_name(), coach.get_id(), user_display))
                                else:
                                    sorted_list.append('{} - {}'.format(coach.get_name(), user_display))
                            else:
                                sorted_list.append('{} - *{}*'.format(coach.get_name(), coach.get_id()))
                        sorted_list.sort()

                        for value in sorted_list:
                            output_msg += '\n▫ ' + value
                            if len(output_msg) >= 1900:
                                await utils.send_custom_message(channel, output_msg)
                                output_msg = ""
                        if len(output_msg) >= 0:
                            await utils.send_custom_message(channel, output_msg)
                    else:
                        # /* No coach in {competition} */
                        raise DefaultError(Translator.tr('#_common_command.no_coach_error', settings.get_language()).format(competition=competition_name))
                else:
                    raise DefaultError(Translator.tr('#_exception.api_unreachable', settings.get_language()))
            else:
                raise CompetitionNotFoundError(settings.get_language(), competition_name, platform)
        else:
            raise CompetitionNotFoundError(settings.get_language(), competition_name, platform)

        await utils.delete_message(ctx.message)

    async def prune(self, ctx, nb_message, user):
        spike_logger.info('Prune command: **{}** by: **{}**'.format(ctx.message.content, ctx.message.author.name))
        spike_logger.info('Command url: {}'.format(ctx.message.jump_url))
        utils = DiscordUtilities(self.client)
        await utils.delete_message(ctx.message)
        rsrc = Resources()

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        if user is None:
            deleted = await ctx.message.channel.purge(limit=nb_message)
            # /* {number} messages deleted */
            await utils.send_custom_message(ctx.message.channel, Translator.tr('#_common_command.message_deleted', settings.get_language()).format(number=len(deleted)), rsrc.get_remove_emoji())
        else:
            user_id = Utilities.get_discord_id(user)

            def check(m):
                return m.author.id == user_id

            deleted = await ctx.message.channel.purge(limit=nb_message, check=check)
            # /* {number} messages deleted from {author} */
            await utils.send_custom_message(ctx.message.channel, Translator.tr('#_common_command.message_deleted_from', settings.get_language()).format(number=len(deleted), author=user), rsrc.get_remove_emoji())

    async def team_list(self, ctx, args):
        channel = ctx.message.channel
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        coach_db = Coaches()
        display_team_value = False
        mention_coach = False
        link_team = False

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        args = args.split(' ')

        platform = Utilities.get_platform_in_args(args)

        if '-tv' in args:
            display_team_value = True
            args.remove('-tv')

        if '-m' in args:
            mention_coach = True
            args.remove('-m')

        if '-link' in args:
            link_team = True
            args.remove('-link')

        if platform is None:
            platform = settings.get_platform()
        else:
            args.remove(platform)

        if platform == 'all':
            platform = 'pc'

        platform_id = common_db.get_platform_id(platform)

        if len(args) > 0:
            competition_name = ' '.join(args)
            # Get all competition to fuzzy the best match
            competitions = db.get_reported_competitions_label(platform_id)
            fuzzy = FuzzyUtils()
            comp_name = fuzzy.get_best_match_into_list(competition_name, competitions)
            if comp_name is not None:
                competition_name = comp_name[0]
                competition_id = db.get_competition_id(competition_name, platform_id)
                if competition_id is not None:
                    competition_data = db.get_competition_data(competition_id)
                    league_name = db.get_league_name(competition_data[7], platform_id)

                    team_list = CyanideApi.get_teams(league_name, competition_name)
                    team_list = team_list.get('teams', [])
                    nb_team = len(team_list)
                    if nb_team > 0:
                        # /* {logo} __**{number} teams in {competition}**__ {logo} */
                        output_msg = Translator.tr('#_common_command.team_list_header', settings.get_language()).format(logo=competition_data[5], number=nb_team, competition=competition_name)

                        teams = []
                        for team in team_list:
                            emoji = common_db.get_team_emoji(team['logo'])
                            line = '{} {}'.format(emoji, team.get('team'))
                            team_model = None
                            if display_team_value:
                                team_model = RequesterUtils.get_team(team_id=team.get('id'), platform_id=platform_id, cached_data=False)
                                if team_model is not None:
                                    line += ' :: *{}*'.format(team_model.get_team_value())

                            line += ' :: **{}**'.format(team.get('coach', 'AI'))

                            if mention_coach:
                                if team_model is None:
                                    team_model = RequesterUtils.get_team(team_id=team.get('id'), platform_id=platform_id, cached_data=True)
                                    if team_model is None:
                                        team_model = RequesterUtils.get_team(team_id=team.get('id'), platform_id=platform_id, cached_data=False)

                                if team_model is not None:
                                    discord_ids = coach_db.get_discord_id(team_model.get_coach().get_id(), platform_id)
                                    if discord_ids is not None and len(discord_ids) > 0:
                                        guild = self.client.get_guild(ctx.guild.id)
                                        if guild is not None:
                                            member = guild.get_member(discord_ids[0])
                                            if member is not None:
                                                line += ' :: {}'.format(member.mention)
                                            else:
                                                line += ' :: **{}**'.format(team.get('coach', 'AI'))
                                        else:
                                            line += ' :: **{}**'.format(team.get('coach', 'AI'))
                                    else:
                                        line += ' :: **{}**'.format(team.get('coach', 'AI'))
                                else:
                                    line += ' :: **{}**'.format(team.get('coach', 'AI'))

                            if link_team:
                                s = pyshorteners.Shortener()
                                try:
                                    url = s.dagd.short('https://spike.ovh/team?team_id={}&platform={}'.format(team.get('id'), platform_id))
                                except:
                                    url = 'https://spike.ovh/team?team_id={}&platform={}'.format(team.get('id'), platform_id)
                                line += ' :: <{}>'.format(url)

                            teams.append(line)

                        for value in teams:
                            output_msg += '\n▫ ' + value
                            if len(output_msg) >= 1800:
                                await DiscordUtilities.send_custom_message(channel, output_msg)
                                output_msg = ""
                        if len(output_msg) >= 0:
                            await DiscordUtilities.send_custom_message(channel, output_msg)
                    else:
                        # /* No team found in **{competition}** */
                        raise DefaultError(Translator.tr('#_common_command.no_team_found_error', settings.get_language()).format(competition=competition_name))
                else:
                    raise CompetitionNotFoundError(settings.get_language(), competition_name, platform)
            else:
                raise CompetitionNotFoundError(settings.get_language(), competition_name)
        else:
            raise InvalidSyntaxError(settings.get_language(), args)
        await DiscordUtilities.delete_message(ctx.message)

    @staticmethod
    async def invite(ctx):
        rsrc = Resources()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        spike_logger.info('User {} - {} want to invite me on his server'.format(ctx.message.author.name, ctx.message.author.mention))
        url = 'https://discordapp.com/oauth2/authorize?client_id=466883259607941150&scope=bot&permissions=268958793'
        discord_url = 'https://discord.gg/8AJh4hx'
        # /* {logo} You can invite me on your discord server with the link below:\n{invite_url}\nFeel free to join my discord too.\n{discord_url} */
        message = Translator.tr('#_common_command.discord_invite', settings.get_language()).format(logo=rsrc.get_discord_emoji(), invite_url=url, discord_url=discord_url)
        await DiscordUtilities.send_custom_message(channel=ctx.message.channel, message=message)
        await DiscordUtilities.delete_message(ctx.message)

    @staticmethod
    async def join(ctx):
        await DiscordUtilities.send_custom_message(channel=ctx.message.channel, message='https://discord.gg/8AJh4hx')
        await DiscordUtilities.delete_message(ctx.message)
