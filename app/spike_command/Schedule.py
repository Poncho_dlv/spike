#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from datetime import datetime

import discord
from fuzzywuzzy import fuzz
from pytz import timezone

from discord_resources import Resources
from discord_resources.Planning import Planning
from discord_resources.Utilities import DiscordUtilities
from discord_write.Scheduling import Scheduling
from spike_command.BaseCommand import BaseCommand
from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_database.DiscordGuild import DiscordGuild
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_exception import DefaultError, NoCompetitionRegisteredError, NoMatchFoundError, InvalidSyntaxError, ContestNotFound, \
    CompetitionNotFoundError, LeagueNotFoundError, CoachNotFoundError
from spike_model.Competition import Competition
from spike_requester.CyanideApi import CyanideApi
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from spike_user_db.Users import Users
from spike_utilities.FuzzyUtils import FuzzyUtils
from spike_utilities.Utilities import Utilities


class Schedule(BaseCommand):

    def __init__(self, client):
        super(Schedule, self).__init__(client)

    async def match_of_the_day(self, ctx):
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')
        utils = DiscordUtilities(self.client)
        contest_db = Contests()
        rsrc_db = ResourcesRequest()
        db = DiscordGuild(ctx.guild.id)

        competitions = db.get_reported_competitions()
        competitions_data = []
        for competition in competitions:
            competition = db.get_competition_data(competition)
            competitions_data.append({'league_id': competition[7], 'competition_id': competition[1], 'platform_id': competition[6]})

        if len(competitions_data) > 0:
            contests = contest_db.get_contest_in_competitions(competitions_data)
            if contests is not None:
                at_least_one_contest = False
                for contest in contests:
                    match_date = contest['date']
                    display_contest = False
                    input_tz = timezone('UTC')
                    today = datetime.now(input_tz)

                    try:
                        if '??:??' in match_date:
                            match_date = datetime.strptime(match_date, '%Y-%m-%d ??:??')
                            output_format = '%d/%m :: ??:??'
                        elif ':??' in match_date:
                            match_date = datetime.strptime(match_date, '%Y-%m-%d %H:??')
                            output_format = '%d/%m :: %H:??'
                        else:
                            match_date = datetime.strptime(match_date, '%Y-%m-%d %H:%M')
                            output_format = '%d/%m :: %H:%M'

                        if match_date.day == today.day:
                            display_contest = True
                            at_least_one_contest = True
                            match_date = input_tz.localize(match_date, True)
                            output_tz = timezone(settings.get_timezone())
                            match_date = '{} *{}*'.format(match_date.astimezone(output_tz).strftime(output_format), settings.get_timezone())
                    except:
                        pass

                    if display_contest:
                        competition_logo = contest['competition_logo']
                        competition = contest['competition_name']
                        home_coach = contest['team_home']['coach_name']
                        home_logo = rsrc_db.get_team_emoji(contest['team_home']['team_logo'])
                        away_coach = contest['team_away']['coach_name']
                        away_logo = rsrc_db.get_team_emoji(contest['team_away']['team_logo'])
                        message = '{} __**{}**__\n{} :: **{}** {} vs {} **{}**'.format(competition_logo, competition, match_date, home_coach,
                                                                                       home_logo, away_logo, away_coach)
                        await utils.send_custom_message(channel, message)

                await utils.delete_message(ctx.message)
                if not at_least_one_contest:
                    # /* No match planned today! */
                    raise DefaultError(Translator.tr('#_schedule_command.no_match_planned_today_error', settings.get_language()))
            else:
                await utils.delete_message(ctx.message)
                # /* No match planned today! */
                raise DefaultError(Translator.tr('#_schedule_command.no_match_planned_today_error', settings.get_language()))
        else:
            await utils.delete_message(ctx.message)
            raise NoCompetitionRegisteredError(settings.get_language())

    @staticmethod
    async def my_sched(ctx):
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')
        competition_db = Competitions()
        rsrc_db = ResourcesRequest()
        contest_db = Contests()
        user_db = Users()
        user = user_db.get_discord_user(ctx.message.author.id)
        output_msg = ""

        if user is None:
            link_label = Translator.tr('#_common_command.link_bb2_discord', settings.get_language())
            # /* You have to link your discord account to use this command.\nYou can do it with the command ``s!info coach name`` and click on *{
            # link}* */ 
            raise DefaultError(Translator.tr('#_common_command.account_not_linked_error', settings.get_language()).format(link=link_label))

        for coach in user.get('blood_bowl_coaches', []):
            contests = contest_db.get_contest_data_with_coach(coach['coach_id'], coach['platform_id'])

            for contest in contests:
                current_round = competition_db.get_current_round(contest['competition_id'], coach['platform_id'], 1)
                if current_round is not None and contest['current_round'] == current_round:
                    if contest.get('date') is not None and contest.get('date') != "":
                        try:
                            date = datetime.strptime(contest['date'], '%Y-%m-%d %H:%M')
                            saved_tz = timezone('UTC')
                            date = saved_tz.localize(date, True)
                            output_tz = timezone(settings.get_timezone())
                            date = '{} *{}*'.format(date.astimezone(output_tz).strftime('%d/%m :: %H:%M'), settings.get_timezone())
                        except:
                            date = Translator.tr('#_schedule_command.not_planed', settings.get_language())
                    else:
                        date = Translator.tr('#_schedule_command.not_planed', settings.get_language())

                    competition_logo = contest['competition_logo']
                    competition = contest['competition_name']
                    home_coach = contest['team_home']['coach_name']
                    home_logo = rsrc_db.get_team_emoji(contest['team_home']['team_logo'])
                    if home_coach is None:
                        home_coach = 'AI'
                        home_logo = ""

                    away_coach = contest['team_away']['coach_name']
                    away_logo = rsrc_db.get_team_emoji(contest['team_away']['team_logo'])
                    if away_coach is None:
                        away_coach = 'AI'
                        away_logo = ""
                    url = f"https://spike.ovh/competition?competition={contest['competition_id']}&platform={ contest['platform_id']}"
                    output_msg += f"\n[{competition_logo} **{competition}**]({url})\n{home_coach} {home_logo} vs {away_logo} {away_coach}\n{date}"

        await DiscordUtilities.delete_message(ctx.message)
        if output_msg != "":
            embed = discord.Embed(title=ctx.message.author.name, description=output_msg, color=0x992d22)
            await DiscordUtilities.send_custom_message(channel, embed=embed)
        else:
            raise NoMatchFoundError(settings.get_language(), ctx.message.author.name)

    async def plan_match(self, ctx, args):
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        utils = DiscordUtilities(self.client)
        plan_utils = Planning(self.client)
        competition_db = Competitions()
        rsrc_db = ResourcesRequest()
        contest_db = Contests()
        user_db = Users()

        try:
            match = re.search(r'(\d+/\d+.*:\d{1,2})', args)
            if match is None:
                match = re.search(r'(\d+/\d+.*:[?][?])', args)

            given_date = args[match.regs[0][0]:match.regs[0][1]]
            args = args.replace(given_date, "")
        except:
            # /* valid date format:\n DD/MM :: HH:MM\n DD/MM :: ??:??\n DD/MM :: HH:??.\nTimezone *{}* */
            raise InvalidSyntaxError(settings.get_language(),
                                     Translator.tr('#_schedule_command.invalid_date_format', settings.get_language()).format(settings.get_timezone()))

        # Manage old format
        args = args.replace(',', "")

        if len(args) > 0:
            searched_coach = args.strip()
            date = None
            date_format = None
            date_formats = plan_utils.get_date_formats()

            for df in date_formats:
                try:
                    date = datetime.strptime(given_date, df)
                    date_format = df
                    break
                except ValueError:
                    pass

            if date is None:
                # /* valid date format:\n DD/MM :: HH:MM\n DD/MM :: ??:??\n DD/MM :: HH:??.\nTimezone *{}* */
                raise InvalidSyntaxError(settings.get_language(),
                                         Translator.tr('#_schedule_command.invalid_date_format', settings.get_language()).format(
                                             settings.get_timezone()))
            else:
                dt_now = datetime.now(timezone(settings.get_timezone()))
                if date.month < dt_now.month:
                    year = dt_now.year + 1
                else:
                    year = dt_now.year

                date = date.replace(year=year, second=0, microsecond=0)
                input_tz = timezone(settings.get_timezone())
                date = input_tz.localize(date, True)

            current_user = user_db.get_discord_user(ctx.message.author.id)

            if current_user is not None and len(current_user.get('blood_bowl_coaches', [])) > 0:
                contests = []
                for linked_coach in current_user.get('blood_bowl_coaches', []):
                    contests.extend(list(contest_db.get_contest_data_with_coach(linked_coach['coach_id'], linked_coach['platform_id'])))
                matching_contest = None
                best_match = 0
                if len(contests) > 0:
                    for contest in contests:
                        current_round = competition_db.get_current_round(contest.get('competition_id'), contest.get('platform_id'))
                        if contest['current_round'] == current_round:
                            if next((item for item in current_user.get('blood_bowl_coaches', []) if
                                     item['coach_id'] == contest['team_home']['coach_id']), None):
                                opponent = contest['team_away']['coach_name']
                            else:
                                opponent = contest['team_home']['coach_name']
                            ratio = fuzz.ratio(opponent, searched_coach)

                            if ratio > best_match:
                                best_match = ratio
                                matching_contest = contest
                    if best_match > 55 and matching_contest is not None:
                        output_tz = timezone('UTC')
                        utc_date = date.astimezone(output_tz)
                        if '??:??' in date_format:
                            utc_date = utc_date.strftime('%Y-%m-%d ??:??')
                        elif ':??' in date_format:
                            utc_date = utc_date.strftime('%Y-%m-%d %H:??')
                        else:
                            utc_date = utc_date.strftime('%Y-%m-%d %H:%M')

                        contest_db.update_contest_date(matching_contest['contest_id'], matching_contest['platform_id'], utc_date)
                        competition_logo = competition_db.get_competition_logo(matching_contest['competition_id'], matching_contest['platform_id'])
                        if competition_logo is None:
                            league_db = Leagues()
                            competition_logo = league_db.get_league_logo(matching_contest['league_id'], matching_contest['platform_id'])
                            competition_logo = rsrc_db.get_team_emoji(competition_logo)
                        if competition_logo is None:
                            competition_logo = Resources.get_blood_bowl_2_emoji()

                        competition = matching_contest['competition_name']
                        home_coach = matching_contest['team_home']['coach_name']
                        home_logo = rsrc_db.get_team_emoji(matching_contest['team_home']['team_logo'])
                        away_coach = matching_contest['team_away']['coach_name']
                        away_logo = rsrc_db.get_team_emoji(matching_contest['team_away']['team_logo'])
                        # /* {competition_logo} **{competition_name}**\n{home_coach} {home_logo} vs {away_logo} {away_coach}\n**Successfully
                        # planned** {date} *{timezone}* */
                        output = Translator.tr('#_schedule_command.contest_successfully_planned', settings.get_language()).format(
                            competition_logo=competition_logo, competition_name=competition, home_coach=home_coach, home_logo=home_logo,
                            away_logo=away_logo, away_coach=away_coach, date=date.strftime(date_format), timezone=settings.get_timezone())
                        await utils.send_custom_message(channel, message=output)

                        for message in matching_contest.get('messages', []):
                            channel_id = message['channel_id']
                            message_id = message['message_id']
                            channel = self.client.get_channel(channel_id)
                            if channel is not None:
                                current_set = DiscordGuildSettings(channel.guild.id)
                                try:
                                    msg = await channel.get_message(message_id)
                                    if msg is not None:
                                        embed = msg.embeds[0]
                                        if embed is not None:
                                            server_tz = timezone(current_set.get_timezone())
                                            date_planned = date.astimezone(server_tz)
                                            embed.description = '{} *{}*'.format(date_planned.strftime(date_format), current_set.get_timezone())
                                            await msg.edit(embed=embed)
                                except:
                                    continue
                        await utils.delete_message(ctx.message)
                    else:
                        raise CoachNotFoundError(settings.get_language(), searched_coach)
                else:
                    raise ContestNotFound(settings.get_language(), current_user.get('user_name'))
            else:
                link_label = Translator.tr('#_common_command.link_bb2_discord', settings.get_language())
                raise DefaultError(Translator.tr('#_common_command.account_not_linked_error', settings.get_language()).format(link=link_label))
        else:
            # /* Give a valid coach name and date */
            raise InvalidSyntaxError(settings.get_language(), Translator.tr('#_common_command.invalid_coach_date_error', settings.get_language()))

    async def schedule_competition(self, ctx, args, calendar_mode=False):
        utils = DiscordUtilities(self.client)
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        output_option = {
            'display_bet': False,
            'display_bnty': False,
            'display_team_name': False,
            'display_date': False,
            'mention_coach': False,
            'calendar_mode': calendar_mode
        }
        args = args.split(' ')

        platform = Utilities.get_platform_in_args(args)

        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if '-d' in args:
            output_option['display_date'] = True
            # Remove -d from args if specified
            args.remove('-d')

        if '-t' in args:
            output_option['display_team_name'] = True
            # Remove -t from args if specified
            args.remove('-t')

        if '-bet' in args:
            output_option['display_bet'] = True
            # Remove -bet from args if specified
            args.remove('-bet')

        if '-bnty' in args:
            output_option['display_bnty'] = True
            # Remove -bnty from args if specified
            args.remove('-bnty')

        if '-m' in args:
            output_option['mention_coach'] = True
            # Remove -m from args if specified
            args.remove('-m')

        if len(args) > 0:
            competition_name = ' '.join(args)

            if platform == 'all':
                platform = 'pc'

            platform_id = common_db.get_platform_id(platform)
            competitions = db.get_reported_competitions_label(platform_id)
            fuzzy = FuzzyUtils()
            comp_name = fuzzy.get_best_match_into_list(competition_name, competitions)
            if comp_name is not None:
                comp_name = comp_name[0]
                await self.display_sched_comp(ctx, comp_name, platform_id, output_option)
            else:
                raise CompetitionNotFoundError(settings.get_language(), competition_name)
        else:
            # /* No competition name found in argument list. */
            raise InvalidSyntaxError(settings.get_language(),
                                     Translator.tr('#_common_command.no_competition_in_arguments_error', settings.get_language()))

        await utils.delete_message(ctx.message)

    async def schedule_league(self, ctx, args, calendar_mode=False):
        utils = DiscordUtilities(self.client)
        db = DiscordGuild(ctx.guild.id)
        common_db = ResourcesRequest()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')
        output_option = {
            'display_bet': False,
            'display_bnty': False,
            'display_team_name': False,
            'display_date': False,
            'mention_coach': False,
            'calendar_mode': calendar_mode
        }
        args = args.split(' ')

        platform = Utilities.get_platform_in_args(args)

        if platform is None:
            platform = settings.get_platform()
        else:
            # Remove platform from args if specified
            args.remove(platform)

        if '-d' in args:
            output_option['display_date'] = True
            # Remove -d from args if specified
            args.remove('-d')

        if '-t' in args:
            output_option['display_team_name'] = True
            # Remove -t from args if specified
            args.remove('-t')

        if '-bet' in args:
            output_option['display_bet'] = True
            # Remove -bet from args if specified
            args.remove('-bet')

        if '-bnty' in args:
            output_option['display_bnty'] = True
            # Remove -bnty from args if specified
            args.remove('-bnty')

        if '-m' in args:
            output_option['mention_coach'] = True
            # Remove -m from args if specified
            args.remove('-m')

        if len(args) > 0:
            league_name = ' '.join(args)

            if platform == 'all':
                platform = 'pc'

            platform_id = common_db.get_platform_id(platform)
            platform = common_db.get_platform(platform_id)

            # Get all competition to fuzzy the best match
            leagues = db.get_reported_leagues_label(platform_id)
            fuzzy = FuzzyUtils()
            lg_name = fuzzy.get_best_match_into_list(league_name, leagues)
            if lg_name is not None:
                league_name = lg_name[0]
                league_id = db.get_league_id(league_name, platform_id)
                league_data = db.get_league_data(league_id)
                if league_data is not None:
                    competitions = db.get_competitions_to_report(league_data[1], platform_id)
                    for competition in competitions:
                        competition_name = db.get_competition_name(competition, platform_id)
                        try:
                            await self.display_sched_comp(ctx, competition_name, platform_id, output_option)
                        except CompetitionNotFoundError:
                            pass  # Ignore empty competition
            else:
                raise LeagueNotFoundError(settings.get_language(), league_name, platform)
        else:
            # /* No league name found in argument list. */
            raise InvalidSyntaxError(settings.get_language(), Translator.tr('#_common_command.no_league_in_arguments_error', settings.get_language()))

        await utils.delete_message(ctx.message)

    async def display_sched_comp(self, ctx, competition_name, platform_id, output_option):
        utils = DiscordUtilities(self.client)
        db = DiscordGuild(ctx.guild.id)
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')
        league_db = Leagues()
        competition_db = Competitions()
        common_db = ResourcesRequest()
        rsrc = Resources()
        channel = ctx.message.channel
        competition_found = False
        platform_data = common_db.get_platform(platform_id)

        competition_id = db.get_competition_id(competition_name, platform_id)
        if competition_id is not None:
            competition_data = db.get_competition_data(competition_id)
            league_name = db.get_league_name(competition_data[7], platform_id)
            ingame_competitions = CyanideApi.get_competitions(league_name, limit=100, platform=platform_data[1])

            if ingame_competitions is not None:
                for currentCompetition in ingame_competitions.get('competitions', []):
                    competition = Competition(currentCompetition)

                    if competition.get_id() is not None:
                        cmp_name = competition.get_name()
                        cmp_status = competition.get_status()
                        lg_name = competition.get_league().get_name()
                        cmp_round = competition.get_current_round()
                        limit = competition.get_teams_count()
                        cmp_format = competition.get_format()

                        league_db.add_or_update_league(lg_name, competition.get_league().get_id(), platform_id, competition.get_league().get_logo())
                        competition_db.add_or_update_raw_competition(competition, platform_id)

                        if cmp_name == competition_name:
                            competition_found = True
                            if cmp_status == 1:
                                if cmp_format != 'ladder':
                                    scheduled_games = CyanideApi.get_contests(league_name=lg_name, competition_name=cmp_name, limit=limit,
                                                                              round_number=cmp_round, platform=platform_data[1])
                                    if scheduled_games is None:
                                        scheduled_games = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(),
                                                                                  platform=platform_data[1], limit=limit, exact=0)

                                    if scheduled_games:
                                        scheduled_games = scheduled_games.get('upcoming_matches', [])
                                        if len(scheduled_games) > 0:
                                            writer = Scheduling(self.client)
                                            if output_option['calendar_mode']:
                                                await writer.write_calendar_mode(ctx, scheduled_games, competition, platform_id, output_option)
                                            else:
                                                await writer.write(ctx, scheduled_games, competition, platform_id, output_option)
                                        else:
                                            cmp_name = '**{}**'.format(cmp_name)
                                            cmp_round = '**{}**'.format(cmp_round)
                                            # /* *All matches of {competition_name} round {cmp_round} played.* */
                                            output = Translator.tr('#_schedule_command.all_matches_played_error', settings.get_language()).format(
                                                competition_name=cmp_name, cmp_round=cmp_round)
                                            await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())
                                    else:
                                        await utils.send_custom_message(channel,
                                                                        Translator.tr('#_exception.api_unreachable', settings.get_language()),
                                                                        rsrc.get_remove_emoji())
                                else:
                                    cmp_name = '**{}**'.format(cmp_name)
                                    # /* Competition {competition_name} is a ladder you can't schedule it. */
                                    output = Translator.tr('#_schedule_command.competition_{competition_name}_is_a_ladder_error',
                                                           settings.get_language()).format(competition_name=cmp_name)
                                    await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())
                            else:
                                cmp_name = '**{}**'.format(cmp_name)
                                # /* Competition {competition_name} not running. */
                                output = Translator.tr('#_schedule_command.competition_{competition_name}_not_running_error',
                                                       settings.get_language()).format(competition_name=cmp_name)
                                await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())

            if not competition_found:
                raise CompetitionNotFoundError(settings.get_language(), competition_name, league_name, platform_data[1])
        else:
            raise CompetitionNotFoundError(settings.get_language(), competition_name, None, platform_data[1])
