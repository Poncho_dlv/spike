#!/usr/bin/env python
# -*- coding: utf-8 -*-
import discord
import feedparser

from __init__ import spike_logger
from discord_resources import Resources
from spike_command.BaseCommand import BaseCommand
from spike_database.DiscordGuild import DiscordGuild
from spike_database.TwitchChannel import TwitchChannel
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from spike_twitch.Utilities import Utilities
from discord_resources.Utilities import DiscordUtilities


class Video(BaseCommand):

    def __init__(self, client):
        super(Video, self).__init__(client)
        self.__youtube_base_url = 'https://www.youtube.com/channel/'
        self.__twitch_base_url = 'https://www.twitch.tv/'

    async def add_twitch_channel(self, ctx, channel_name):
        twitch_db = TwitchChannel()
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        rsrc = Resources()

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        user_exist, data = Utilities.user_exist(channel_name)
        try:
            if user_exist:
                twitch_db.register_twitch_channel(int(data[0].get('id')), data[0].get('login'), data[0].get('display_name'), ctx.guild.id)
                # /* Successfully added: */
                add_success_label = Translator.tr('#_video_command.successfully_added', settings.get_language())
                output = '{} {}\n{}{}'.format(rsrc.get_twitch_emoji(), add_success_label, self.__twitch_base_url, channel_name)
            else:
                # /* Invalid channel: */
                invalid_channel_label = Translator.tr('#_video_command.invalid_channel')
                output = '{} {} {}'.format(rsrc.get_twitch_emoji(), invalid_channel_label, channel_name)

            await utils.send_custom_message(channel, output)
        except Exception as e:
            spike_logger.error('Error in add_twitch_channel: {}'.format(e))
            # /* Unable to add: {channel_name} */
            output = Translator.tr('#_video_command.unable_to_add_channel').format(channel_name=channel_name)
            rsrc = Resources()
            await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())

        await utils.delete_message(ctx.message)

    async def add_youtube_channel(self, ctx, channel_id):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        rsrc = Resources()

        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        db = DiscordGuild(ctx.guild.id)
        try:
            rss_url = 'https://www.youtube.com/feeds/videos.xml?channel_id={}'.format(channel_id)
            rss = feedparser.parse(rss_url)
            if rss.status == 200:
                db.add_video_channel(channel_id)
                add_success_label = Translator.tr('#_video_command.successfully_added', settings.get_language())
                output = '{} {}\n{}{}'.format(rsrc.get_youtube_emoji(), add_success_label, self.__youtube_base_url, channel_id)
            else:
                invalid_channel_label = Translator.tr('#_video_command.invalid_channel')
                output = '{} {} {}'.format(rsrc.get_youtube_emoji(), invalid_channel_label, channel_id)
            await utils.send_custom_message(channel, output)
        except Exception as e:
            spike_logger.error('Error in add_youtube_channel: {}'.format(e))
            output = Translator.tr('#_video_command.unable_to_add_channel', settings.get_language()).format(channel_name=channel_id)
            rsrc = Resources()
            await utils.send_custom_message(channel, output, rsrc.get_remove_emoji())

        await utils.delete_message(ctx.message)

    async def videos(self, ctx):
        utils = DiscordUtilities(self.client)
        rsrc = Resources()
        channel = ctx.message.channel
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        try:
            db = DiscordGuild(ctx.guild.id)
            list_of_channel = db.get_video_channel()
            # /* Followed channels */
            follow_label = Translator.tr('#_video_command.followed_channels', settings.get_language())
            if len(list_of_channel) > 0:
                title = '__**{logo} {follow_label} {logo}:**__'.format(logo=rsrc.get_youtube_emoji(), follow_label=follow_label)
                output_msg = ""
                for ytChannel in list_of_channel:
                    rss_url = 'https://www.youtube.com/feeds/videos.xml?channel_id={}'.format(ytChannel)
                    rss = feedparser.parse(rss_url)
                    if rss.status == 200:
                        channel_name = rss.feed.author
                        output_msg += '\n▫[{}]({}{})'.format(channel_name, self.__youtube_base_url, ytChannel)
                    else:
                        invalid_channel_label = Translator.tr('#_video_command.invalid_channel')
                        await utils.send_custom_message(channel, message='{} {} {}'.format(rsrc.get_youtube_emoji(), invalid_channel_label, ytChannel))
                embed = discord.Embed(title=title, description=output_msg, color=0x992d22)
                await utils.send_custom_message(channel, embed=embed)

            twitch_db = TwitchChannel()
            list_of_channel = twitch_db.get_registered_channel(ctx.guild.id)
            if len(list_of_channel) > 0:
                title = '__**{} {}:**__'.format(rsrc.get_twitch_emoji(), follow_label)
                output_msg = ""
                for twChannel in list_of_channel:
                    output_msg += '\n▫[{}]({}{})'.format(twChannel.get('display_name'), self.__twitch_base_url, twChannel.get('user_name'))
                embed = discord.Embed(title=title, description=output_msg, color=0x6033c4)
                await utils.send_custom_message(channel, embed=embed)

        except Exception as e:
            spike_logger.error('Error in videos: {}'.format(e))
            # /* Unable to get channel list */
            output = Translator.tr('#_video_command.unable_to_get_video_list')
            await utils.send_custom_message(channel, output)
        await utils.delete_message(ctx.message)

    async def unregister_twitch_channel(self, ctx, channel_name):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        rsrc = Resources()
        twitch_db = TwitchChannel()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')
        user_exist, data = Utilities.user_exist(channel_name)

        try:
            if user_exist:
                twitch_db.unregister_twitch_channel(int(data[0].get('id')), ctx.guild.id)
                # /* Successfully removed */
                removed_label = Translator.tr('#_video_command.channel_removed_ok', settings.get_language())
                output = '{} {}:\n{}{}'.format(rsrc.get_twitch_emoji(), removed_label, self.__twitch_base_url, channel_name)
                await utils.send_custom_message(channel, output)
        except Exception as e:
            spike_logger.error('Error in remove_video_channel: {}'.format(e))
            output = Translator.tr('#_common_command.unable_to_remove_entry', settings.get_language()).format(entry=channel_name)
            await utils.send_custom_message(channel, output)
        await utils.delete_message(ctx.message)

    async def remove_youtube_channel(self, ctx, channel_id):
        utils = DiscordUtilities(self.client)
        channel = ctx.message.channel
        rsrc = Resources()
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        try:
            db = DiscordGuild(ctx.guild.id)
            db.remove_video_channel(channel_id)
            removed_label = Translator.tr('#_video_command.channel_removed_ok', settings.get_language())
            output = '{} {}:\n{}{}'.format(rsrc.get_youtube_emoji(), removed_label, self.__youtube_base_url, channel_id)
            await utils.send_custom_message(channel, output)
        except Exception as e:
            spike_logger.error('Error in remove_youtube_channel: {}'.format(e))
            output = Translator.tr('#_common_command.unable_to_remove_entry', settings.get_language()).format(entry=channel_id)
            await utils.send_custom_message(channel, output)
        await utils.delete_message(ctx.message)
