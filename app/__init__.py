#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import discord

intents = discord.Intents.all()

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

spike_logger = logging.getLogger("spike_logger")
formatter = logging.Formatter(fmt="%(asctime)s :: %(levelname)s :: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z")
spike_logger.setLevel(logging.DEBUG)
spike_logger.addHandler(stream_handler)


spike_requester_logger = logging.getLogger("spike_cyanide_requester")
spike_requester_formatter = logging.Formatter(fmt="%(asctime)s :: %(levelname)s :: BOT :: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z")
spike_requester_logger.setLevel(logging.INFO)
spike_requester_logger.addHandler(stream_handler)

spike_command_logger = logging.getLogger("spike_command_logger")


def except_hook(except_type, value, traceback):
    spike_logger.critical(except_type)
    spike_logger.critical(value)
    spike_logger.critical(traceback)
