#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from discord.ext.commands import Bot
import discord

from __init__ import spike_logger, except_hook
from spike_event.EventCommon import EventCommon
from spike_settings.SpikeSettings import SpikeSettings
intents = discord.Intents.none()
intents.guilds = True
DEBUG_MODE = False

# Discord client to communicate with server
client = Bot(command_prefix="", intents=intents)

locked_user = []
Already_running = False


def main():
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not DEBUG_MODE and not Already_running:
        spike_logger.info("{} event connected".format(client.user.name))
        Already_running = True
        event = EventCommon(client)
        await event.on_ready()
    else:
        spike_logger.warning("{} event reconnect".format(client.user.name))


@client.event
async def on_guild_join(server):
    spike_logger.info("{} guild join: {} - {}".format(client.user.name, server.name, server.id))
    event = EventCommon(client)
    await event.on_guild_join(server)


@client.event
async def on_guild_remove(server):
    spike_logger.info("{} guild remove: {} - {}".format(client.user.name, server.name, server.id))
    event = EventCommon(client)
    await event.on_guild_remove(server)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
