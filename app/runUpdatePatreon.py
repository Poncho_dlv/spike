#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import discord

from __init__ import spike_logger, except_hook, intents
from spike_settings.SpikeSettings import SpikeSettings
from spike_database.DiscordGuildInfo import DiscordGuildInfo

client = discord.Client(intents=intents)

# Avoid restart the update in case of web socket restart
Already_running = False


def main():
    client.run(SpikeSettings.get_patreon_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        Already_running = True
        await update()


async def update():
    guild_db = DiscordGuildInfo()
    guilds_id = []
    for guild in client.guilds:
        guilds_id.append(guild.id)
    guild_db.update_patreon(guilds_id)
    spike_logger.info("Update finished")
    sys.exit()


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
