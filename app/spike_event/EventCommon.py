#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import os
import traceback

import discord
from discord.ext import commands

import discord_resources
import spike_exception
from __init__ import spike_logger, spike_command_logger
from spike_database.Bounty import Bounty
from spike_database.Coaches import Coaches
from spike_database.DiscordGuild import DiscordGuild
from spike_database.Players import Players
from spike_database.ResourcesRequest import ResourcesRequest
from spike_database.Teams import Teams
from spike_requester.Utilities import Utilities as RequesterUtils
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_translation import Translator
from discord_resources.Utilities import DiscordUtilities, update_guild
from discord_resources.Planning import Planning
from spike_utilities.Utilities import Utilities
from discord_write.Match import Match
from discord_write.Team import Team


class EventCommon:

    def __init__(self, client):
        self.client = client

    async def on_ready(self):
        spike_logger.info("Logged in as: " + self.client.user.name)
        excluded_server = Utilities.get_excluded_server()

        nb_server = len(self.client.guilds) - len(excluded_server)

        # Display starting log in Spike discord
        spike_logger.info("======================================================")
        spike_logger.info("{} started...".format(self.client.user.name))
        spike_logger.info("Servers: {}".format(nb_server))
        spike_logger.info("Init servers:")
        for guild in self.client.guilds:
            if guild.id not in excluded_server:
                self.init_settings_and_database(guild)
        spike_logger.info("Spike initialized successfully")
        spike_logger.info("======================================================")

    async def on_guild_remove(self, guild):
        spike_logger.info("{} Leave server: {} :: {}".format(self.client.user.name, guild.name, guild.id))

        utils = DiscordUtilities(self.client)
        leave_server_message = "{} leave server: {} :: {}".format(self.client.user.name, guild.name, guild.id)
        leave_server_message += "\nOwner: {} - {}\n".format(guild.owner.name, guild.owner.mention)
        log_channel = self.client.get_channel(467468994853732353)
        await utils.send_custom_message(log_channel, leave_server_message)
        spike_logger.debug("OUT on_guild_remove")

    async def on_guild_join(self, guild):
        spike_logger.info("{} Join server: {} :: {}".format(self.client.user.name, guild.name, guild.id))
        utils = DiscordUtilities(self.client)
        await update_guild(guild)
        self.init_settings_and_database(guild)

        join_server_message = "{} join a new server: {} :: {} ".format(self.client.user.name, guild.name, guild.id)
        join_server_message += "\nOwner: {} - {}\n".format(guild.owner.name, guild.owner.mention)
        log_channel = self.client.get_channel(467468994853732353)
        await utils.send_custom_message(log_channel, join_server_message)

        join_msg = "Hey!\n I'm Spike, the Blood Bowl 2 bot, I was invited on your server, if you want to configure me, you can read the short step by step bellow!\n"
        join_msg += "```md\n#Step By Step#\n" \
                    "1. Register your league with the command : /add-league\n" \
                    "2. Register your competitions with the command /add-competition\n" \
                    "3. Check league and competitions registration with the command /competitions```"
        #join_msg += "\nMore detail: https://wiki.spike.ovh/bin/view/Quick%20Start/"

        await utils.send_custom_message(guild.owner, join_msg)

    async def on_raw_reaction_add(self, raw_reaction, locked_user):
        try:
            if raw_reaction.user_id != self.client.user.id:
                if raw_reaction.emoji.is_custom_emoji():
                    bnty_emoji = discord.utils.get(self.client.emojis, name="bnty")
                    detail_emoji = discord.utils.get(self.client.emojis, name="More")
                    if raw_reaction.emoji.id == bnty_emoji.id:
                        await self.manage_bounty(raw_reaction)
                    elif raw_reaction.emoji.id == detail_emoji.id:
                        await self.display_detailed_report(raw_reaction)
                else:
                    rsrc = discord_resources.Resources()
                    if raw_reaction.emoji.name == rsrc.get_remove_emoji():
                        guild = self.client.get_guild(raw_reaction.guild_id)
                        if guild is not None:
                            member = guild.get_member(raw_reaction.user_id)
                            utils = DiscordUtilities(self.client)
                            if utils.member_is_bot_admin(member, raw_reaction.guild_id):
                                channel = self.client.get_channel(raw_reaction.channel_id)
                                msg = await channel.fetch_message(raw_reaction.message_id)
                                if msg.author.id == self.client.user.id:
                                    await utils.delete_message(msg)
                    elif raw_reaction.emoji.name == rsrc.get_calendar_emoji():
                        await self.manage_calendar(raw_reaction, locked_user)
        except Exception as e:
            user = self.client.get_user(raw_reaction.user_id)
            try:
                locked_user.remove(user)
            except ValueError:
                pass
            spike_logger.error("Error in on_raw_reaction_add: {}".format(e))
        return locked_user

    async def on_command_error(self, ctx, error, command_prefix):
        if ctx.guild is not None:
            settings = DiscordGuildSettings(f"{ctx.guild.id}")
        else:
            settings = DiscordGuildSettings('0')

        try:
            rsrc = discord_resources.Resources()
            if hasattr(error, "original") and isinstance(error.original, spike_exception.SpikeError):
                await DiscordUtilities.send_custom_message(ctx.message.channel, error.original.message, rsrc.get_remove_emoji())
            elif isinstance(error, spike_exception.NoPrivateChannelError) or isinstance(error, commands.NoPrivateMessage):
                await DiscordUtilities.send_custom_message(ctx.message.channel, error)
            elif isinstance(error, spike_exception.InvalidRightError):
                await DiscordUtilities.send_custom_message(ctx.message.channel, error.message, rsrc.get_remove_emoji())
            elif isinstance(error, commands.MissingRequiredArgument):
                await self.send_cmd_help(ctx)
            elif isinstance(error, commands.BadArgument):
                await self.send_cmd_help(ctx)
            elif isinstance(error, commands.CommandInvokeError):
                if hasattr(error, "original") and isinstance(error.original, discord.Forbidden):
                    error_message = Translator.tr("#_exception.write_message_forbidden", settings.get_language()).format(channel_name=ctx.message.channel)
                    await DiscordUtilities.send_custom_message(ctx.message.author, error_message)
                elif hasattr(error, "original") and isinstance(error.original, discord.HTTPException):
                    if error.original.code == 50035:
                        error_message = Translator.tr("#_exception.message_too_big", settings.get_language())
                        await DiscordUtilities.send_custom_message(ctx.message.channel, error_message)
                errors = traceback.format_exception(type(error), error, error.__traceback__)
                current_time = datetime.datetime.utcnow().strftime("%b/%d/%Y %H:%M:%S UTC")
                traceback.print_exception(type(error), error, error.__traceback__)
                cog_error = "```fix\nCogs:{}\tCommand:{}\tAuthor:{}\n{}\nError:\n{}```".format(ctx.command.cog_name, ctx.command, ctx.message.author, ctx.message.clean_content, error)
                error_msg = "```py\n{}```\n{}\n```py\n{}\n```".format(current_time + "\n" + "ERROR!", cog_error, "".join(errors))
                error_log_channel = self.client.get_channel(466246060017188865)
                if len(error_msg) < 2000:
                    await DiscordUtilities.send_custom_message(error_log_channel, error_msg)
                else:
                    error_msg = "```py\n{}```\n{}".format(current_time + "\n" + "ERROR!", cog_error)
                    await DiscordUtilities.send_custom_message(error_log_channel, error_msg)
                    error_msg = "```py\n{}\n```".format("".join(errors))
                    await DiscordUtilities.send_custom_message(error_log_channel, error_msg)

                # /* Error occurred, a report has been sent to Poncho dlv */
                await DiscordUtilities.send_custom_message(ctx.message.channel, Translator.tr("#_event_common.error_with_report", settings.get_language()), rsrc.get_remove_emoji())
            else:
                # /* Unknown command, type {}help to get list of available command */
                await DiscordUtilities.send_custom_message(ctx.message.channel, Translator.tr("#_event_common.unknown_command", settings.get_language()).format(command_prefix), rsrc.get_remove_emoji())
        except Exception as e:
            spike_logger.error(e)
            spike_logger.error(error)

    async def on_member_join(self, member):
        try:
            spike_logger.debug("on_member_join: {}".format(member.name))
            spike_logger.debug(str(member.guild.id))
            settings = DiscordGuildSettings(member.guild.id)
            utils = DiscordUtilities(self.client)

            # Welcome message
            if settings.get_welcome_enabled():
                channel_id = settings.get_welcome_message_channel()
                output_channel = self.client.get_channel(channel_id)
                welcome_msg = settings.get_welcome_message()
                welcome_msg = welcome_msg.replace("{server}", member.guild.name).replace("{user}", member.mention)
                await utils.send_custom_message(output_channel, welcome_msg)

                users_to_notify = utils.get_members_by_role(settings.get_welcome_notification_role(), member.guild)
                if len(users_to_notify) > 0:
                    admin_message = "{} - {} joined {}.".format(member.name, member.mention, member.guild.name)
                    for user in users_to_notify:
                        await utils.send_custom_message(user, admin_message)
            try:
                # Auto role
                if settings.get_auto_role_enable():
                    role_id = settings.get_auto_role()
                    role = self.get_role(member.guild.roles, role_id)
                    if role is not None:
                        await member.add_roles(role)
            except discord.Forbidden:
                spike_logger.error("Error with auto role in guild {}: Forbidden".format(member.guild.name))
            except discord.HTTPException:
                spike_logger.error("Error with auto role in guild {}: HTTPException".format(member.guild.name))
            except Exception as e:
                spike_logger.error("Error with auto role in guild {}: {}".format(member.guild.name, str(e)))
        except Exception as e:
            spike_logger.error("Error in on_member_join: {}".format(e))

    async def on_member_remove(self, member):
        try:
            spike_logger.debug("on_member_remove: {}".format(member.name))
            settings = DiscordGuildSettings(member.guild.id)
            if settings.get_welcome_enabled():
                utils = DiscordUtilities(self.client)
                channel = self.client.get_channel(settings.get_leave_message_channel())
                leave_msg = settings.get_leave_message().format(user=member.name, server=member.guild.name)
                await utils.send_custom_message(channel, leave_msg)

                users_to_notify = utils.get_members_by_role(settings.get_welcome_notification_role(), member.guild)
                if len(users_to_notify) > 0:
                    for user in users_to_notify:
                        await utils.send_custom_message(user, leave_msg)
        except Exception as e:
            spike_logger.error("Error in on_member_remove: {}".format(e))

    @staticmethod
    async def send_cmd_help(ctx):
        help_command = "```{}{}\n\n{}```".format(ctx.prefix, ctx.invoked_with, ctx.command.help)
        await DiscordUtilities.send_custom_message(ctx.author, help_command)

    @staticmethod
    def init_settings_and_database(guild):
        try:
            spike_logger.info("Init server: {} :: id: {} :: owner_id: {}".format(guild.name, guild.id, guild.owner_id))
            settings = DiscordGuildSettings(guild.id)
            settings.set_server_name(guild.name)

            # Init db for the new server
            db = DiscordGuild(guild.id)
            db.create_tables()
        except Exception as e:
            spike_logger.error("Error in init_settings_and_database: {}".format(e))

    async def manage_bounty(self, raw_reaction):
        channel = self.client.get_channel(raw_reaction.channel_id)
        if channel is not None:
            msg = await channel.fetch_message(raw_reaction.message_id)
            if msg is not None:
                if len(msg.embeds) > 0:
                    footer = msg.embeds[0].footer
                    if "Player id" in footer.text:
                        await self.save_bounty(raw_reaction)
                    elif "Team id" in footer.text:
                        await self.display_list_of_player(raw_reaction)

    async def display_list_of_player(self, raw_reaction):
        utils = DiscordUtilities(self.client)
        common_db = ResourcesRequest()
        rsrc = discord_resources.Resources()
        player_db = Players()
        guild = self.client.get_guild(raw_reaction.guild_id)
        if guild is not None:
            spike_command_logger.info("bnty_list_of_player :: {}".format(guild.name))
        else:
            spike_command_logger.info("bnty_list_of_player :: Private message")

        user = self.client.get_user(raw_reaction.user_id)
        if user is not None:
            channel = self.client.get_channel(raw_reaction.channel_id)
            msg = await channel.fetch_message(raw_reaction.message_id)
            footer = msg.embeds[0].footer
            footer = footer.text.split(" ")

            platform = Utilities.get_platform_in_args(footer)

            if platform is None:
                platform = "pc"

            platform_id = common_db.get_platform_id(platform)
            team_id = footer[2]

            # TODO set cached_data = True to avoid double call
            team = RequesterUtils.get_team(team_id=team_id, platform_id=platform_id)
            if team is not None:
                writer = Team(self.client, None)
                player_db.add_or_update_players(team.get_players(), platform_id, team.get_id(), True)
                team_logo = common_db.get_team_emoji(team.get_logo())
                title = "{}__**{}**__".format(team_logo, team.get_name())

                for player in team.get_players():
                    description = "**{} - {}**\n".format(player.get_number(), player.get_name())
                    description += writer.get_player_skills(player)
                    description += player.get_casualties_state()
                    # /* Click on {logo} to add a bounty on this player */
                    add_label = Translator.tr("#_event_common.click_add_bounty").format(logo=rsrc.get_bounty_emoji())
                    description += "\n\n{}".format(add_label)
                    bnty_emoji = discord.utils.get(self.client.emojis, name="bnty")
                    embed = discord.Embed(title=title, description=description, color=0x996822)
                    embed.set_footer(text="Player id: {} - {}".format(player.get_id(), platform))
                    await utils.send_custom_message(user, embed=embed, emoji=bnty_emoji)

    async def save_bounty(self, raw_reaction):
        utils = DiscordUtilities(self.client)
        common_db = ResourcesRequest()
        rsrc = discord_resources.Resources()
        bounty_db = Bounty()
        team_db = Teams()
        player_db = Players()
        coach_db = Coaches()
        guild = self.client.get_guild(raw_reaction.guild_id)
        if guild is not None:
            spike_command_logger.info("save_bounty :: {}".format(guild.name))
        else:
            spike_command_logger.info("save_bounty :: Private message")

        user = self.client.get_user(raw_reaction.user_id)
        if user is not None:
            channel = self.client.get_channel(raw_reaction.channel_id)
            msg = await channel.fetch_message(raw_reaction.message_id)
            footer = msg.embeds[0].footer
            footer = footer.text.split(" ")

            platform = Utilities.get_platform_in_args(footer)
            platform_id = common_db.get_platform_id(platform)
            player_id = int(footer[2])
            player = player_db.get_player(player_id, platform_id)
            if player is not None:
                player_name = player.get("name")
                team_id = player.get("team_id")
                team = team_db.get_team(team_id, platform_id)
                team_name = team.get("name")
                team_logo = common_db.get_team_emoji(team.get("logo"))
                coach_name = coach_db.get_coach_name(team.get("coach_id"), platform_id)
                settings = DiscordGuildSettings(raw_reaction.guild_id)
                if not bounty_db.is_bountied_by_me(player_id, platform_id, raw_reaction.user_id):
                    bounty_db.add_or_update_bounty(player, team, platform_id, raw_reaction.user_id, coach_name)
                    # /* {logo} Player **{player}** of {team_logo} **{team_name}** successfully bountied */
                    message = Translator.tr("#_event_common.add_bounty_ok", settings.get_language()).format(logo=rsrc.get_bounty_emoji(), player=player_name, team_logo=team_logo, team_name=team_name)
                else:

                    # /* {logo} Player **{player}** of {team_logo} **{team_name}** already bountied */
                    message = Translator.tr("#_event_common.already_bountied").format(logo=rsrc.get_bounty_emoji(), player=player_name, team_logo=team_logo, team_name=team_name)
                await utils.send_custom_message(user, message)

    async def display_detailed_report(self, raw_reaction):
        utils = DiscordUtilities(self.client)
        guild = self.client.get_guild(raw_reaction.guild_id)
        if guild is not None:
            spike_command_logger.info("detailed_report :: {}".format(guild.name))
        else:
            spike_command_logger.info("detailed_report :: Private message")

        try:
            channel = self.client.get_channel(raw_reaction.channel_id)
            msg = await channel.fetch_message(raw_reaction.message_id)
        except discord.NotFound as e:
            spike_logger.error("Message not found error in display_detailed_report: {}".format(e))
            raise e
        except discord.Forbidden as e:
            spike_logger.error("Forbidden error in display_detailed_report: {}".format(e))
            raise e
        except discord.HTTPException as e:
            spike_logger.error("HTTPException error in display_detailed_report: {}".format(e))
            raise e
        except Exception as e:
            spike_logger.error("Unknown error in display_detailed_report: {}".format(e))
            raise e

        if len(msg.embeds) > 0:
            footer = msg.embeds[0].footer
            footer = footer.text.split(" ")
            user = self.client.get_user(raw_reaction.user_id)
            if len(footer) == 8:
                match_uuid = footer[7]
                match = RequesterUtils.get_match(match_uuid)
                settings = DiscordGuildSettings(raw_reaction.guild_id)
                if match is not None:
                    writer = Match(self.client, raw_reaction.guild_id)
                    try:
                        await writer.write_data(match, False, True, user, True)
                    except Exception as e:
                        # /* Unable to display this report */
                        await utils.send_custom_message(user, Translator.tr("#_common_command.unable_to_display_match_report", settings.get_language()))
                        spike_logger.error("write_data :: match {} :: error: {}".format(match.get_uuid(), e))
                else:
                    await utils.send_custom_message(user, Translator.tr("#_common_command.unable_to_display_match_report", settings.get_language()))

    async def manage_calendar(self, raw_reaction, locked_user):
        user = self.client.get_user(raw_reaction.user_id)
        if raw_reaction.guild_id is not None:
            guild = self.client.get_guild(raw_reaction.guild_id)
            if guild is not None:
                member = guild.get_member(raw_reaction.user_id)
                if member is not None:
                    utils = DiscordUtilities(self.client)
                    if utils.member_is_bot_admin(member, raw_reaction.guild_id):
                        guild = self.client.get_guild(raw_reaction.guild_id)
                        if guild is not None:
                            spike_command_logger.info("manage_calendar :: {}".format(guild.name))
                        if user in locked_user:
                            rsrc = discord_resources.Resources()
                            channel = self.client.get_channel(raw_reaction.channel_id)
                            settings = DiscordGuildSettings(guild.id)
                            # /* Unable to schedule many game at the same time */
                            await utils.send_custom_message(channel, message=Translator.tr("#_schedule_command.many_schedule_error", settings.get_language()), emoji=rsrc.get_remove_emoji())
                        locked_user.append(user)

                        planning_utils = Planning(self.client)
                        await planning_utils.ask_scheduling(raw_reaction)
                        try:
                            locked_user.remove(user)
                        except ValueError:
                            pass
        else:
            try:
                locked_user.remove(user)
            except ValueError:
                pass
            raise spike_exception.NoPrivateChannelError("en")

    @staticmethod
    def get_role(server_roles, role_id):
        for role in server_roles:
            if role.id == role_id:
                return role
        return None
