# Python version
FROM python:latest

COPY ./app /app

# Set the working directory
WORKDIR /app

# Install the dependencies
RUN pip install -r requirements.txt